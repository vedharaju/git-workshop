# Git Workshop

## Installing Git
https://www.atlassian.com/git/tutorials/install-git

## Additional Resources
https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf
http://nvie.com/posts/a-successful-git-branching-model/
https://www.atlassian.com/git/tutorials/

## Interactive Tutorial
http://learngitbranching.js.org/


